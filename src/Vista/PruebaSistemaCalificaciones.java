/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.SistemaCalificaciones;
import Util.ExceptionUFPS;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase de Prueba para la clase Sistema de Calificaciones
 * @author madarme
 */
public class PruebaSistemaCalificaciones {
    
    public static void main(String[] args) {
        
       
        
        //Datos para UN estudiante:
        String nombre="Maria Filomena Jaimes";
        long codigo=1150899;
        String quices="3.5,5,4.8,3.3";
        
        //Definimos el tamaño del arreglo:
        int cantEstudiantes=1;
        
         SistemaCalificaciones sistema=new SistemaCalificaciones(cantEstudiantes);
        
        try {
            //Vamos a crear a un Estudiante, dentro del sistema a partir de la lista de estructuras definida
            sistema.insertarEstudiante_EnPos0(codigo, nombre, quices);
            //Obtener los datos almacenados:
            System.out.println(sistema.toString());
        } catch (ExceptionUFPS ex) {
            System.out.println("Datos No válidos para crear el vector-Mensaje desde el frontend:"+ex.getMessage());
        }
        
        
        
//        String dato="3.4";
//        float unFloat=Float.parseFloat(dato);
//        System.out.println(unFloat);
        
    }
    
}
